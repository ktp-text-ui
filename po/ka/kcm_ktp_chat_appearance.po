# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the ktp-text-ui package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ktp-text-ui\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2022-12-14 18:32+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: appearance-config-tab.cpp:44
#, kde-format
msgid "A demo chat"
msgstr "საჩვენებელი ჩატი"

#: appearance-config-tab.cpp:45
#, kde-format
msgid "Jabber"
msgstr "Jabber"

#: appearance-config-tab.cpp:47 appearance-config-tab.cpp:196
#: appearance-config-tab.cpp:203 appearance-config-tab.cpp:249
#: appearance-config-tab.cpp:265
#, kde-format
msgctxt "Example email"
msgid "ted@example.com"
msgstr "ted@example.com"

#: appearance-config-tab.cpp:48
#, kde-format
msgctxt "Example name"
msgid "Ted"
msgstr "Ted"

#: appearance-config-tab.cpp:181
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Ok!"
msgstr "დიახ!"

#: appearance-config-tab.cpp:182 appearance-config-tab.cpp:189
#: appearance-config-tab.cpp:232 appearance-config-tab.cpp:240
#, kde-format
msgctxt "Example email"
msgid "larry@example.com"
msgstr "larry@example.com"

#: appearance-config-tab.cpp:183 appearance-config-tab.cpp:190
#: appearance-config-tab.cpp:233 appearance-config-tab.cpp:241
#, kde-format
msgctxt "Example name"
msgid "Larry Demo"
msgstr "Larry Demo"

#: appearance-config-tab.cpp:188
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Bye Bye"
msgstr "ნახვამდის"

#: appearance-config-tab.cpp:195
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Have fun!"
msgstr "გაერთე!"

#: appearance-config-tab.cpp:197 appearance-config-tab.cpp:204
#: appearance-config-tab.cpp:210 appearance-config-tab.cpp:217
#: appearance-config-tab.cpp:224 appearance-config-tab.cpp:250
#: appearance-config-tab.cpp:266 appearance-config-tab.cpp:273
#, kde-format
msgctxt "Example name"
msgid "Ted Example"
msgstr "Ted Example"

#: appearance-config-tab.cpp:202
#, kde-format
msgctxt "Example message in preview conversation"
msgid "cya"
msgstr "ნახვამდის"

#: appearance-config-tab.cpp:209
#, kde-format
msgctxt "Example message"
msgid "Ted Example waves."
msgstr "Ted Example ხელს გიქნევთ."

#: appearance-config-tab.cpp:216
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Ted Example has left the chat."
msgstr "Ted Example გავიდა ჩატიდან."

#: appearance-config-tab.cpp:223
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Ted Example has joined the chat."
msgstr "Ted Example შემოუერთდა ჩატს."

#: appearance-config-tab.cpp:231
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Hello Ted"
msgstr "გამარჯობა, ტედ"

#: appearance-config-tab.cpp:239
#, kde-format
msgctxt "Example message in preview conversation"
msgid "What's up?"
msgstr "რა ხდება?"

#: appearance-config-tab.cpp:246
#, kde-format
msgctxt "Example message in preview conversation"
msgid ""
"Check out which cool adium themes work <a href=\"http://community.kde.org/"
"KTp/Components/Chat_Window/Themes\">here</a>!"
msgstr ""
"შეამოწმეთ adium -ის რომელი თემები მუშაობს, <a href=\"http://community.kde."
"org/KTp/Components/Chat_Window/Themes\">აქ</a>!"

#: appearance-config-tab.cpp:256
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Hello"
msgstr "გამარჯობა"

#: appearance-config-tab.cpp:257
#, kde-format
msgctxt "Example email"
msgid "bob@example.com"
msgstr "bob@example.com"

#: appearance-config-tab.cpp:258
#, kde-format
msgctxt "Example name"
msgid "Bob Example"
msgstr "Bob Example"

#: appearance-config-tab.cpp:264
#, kde-format
msgctxt "Example message in preview conversation"
msgid "A different example message"
msgstr "სხვა მაგალითი შეტყობინება"

#: appearance-config-tab.cpp:272
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Ted Example is now Away."
msgstr "Ted Example ახლა გასულია."

#: appearance-config.cpp:47
#, kde-format
msgctxt "@title:tab"
msgid "Normal Chat"
msgstr "ნორმალური ჩატი"

#: appearance-config.cpp:51
#, kde-format
msgctxt "@title:tab"
msgid "Group Chat"
msgstr "ჯგუფური ჩატი"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: appearance-config.ui:41
#, kde-format
msgid "Style"
msgstr "სტილი"

#. i18n: ectx: property (text), widget (QLabel, styleLabel)
#: appearance-config.ui:52
#, kde-format
msgid "Message Style:"
msgstr "შეტყობინების სტილი:"

#. i18n: ectx: property (text), widget (QLabel, variantLabel)
#: appearance-config.ui:78
#, kde-format
msgid "Variant:"
msgstr "ვარიანტი:"

#. i18n: ectx: property (text), widget (QCheckBox, showHeader)
#: appearance-config.ui:113
#, kde-format
msgid "Show Header"
msgstr "თავსართის ჩვენება"

#. i18n: ectx: property (text), widget (QCheckBox, showPresenceCheckBox)
#: appearance-config.ui:129
#, kde-format
msgid "Show status changes"
msgstr "სტატუსის ცვლილებების ჩვენება"

#. i18n: ectx: property (text), widget (QCheckBox, showJoinLeaveCheckBox)
#: appearance-config.ui:142
#, kde-format
msgid "Show when contacts join/leave chat"
msgstr "როცა კონტაქტები შემოდიან/გავლენ ჩატიდან"

#. i18n: ectx: property (title), widget (QGroupBox, customFontBox)
#: appearance-config.ui:170
#, kde-format
msgid "Use Custom Font Settings"
msgstr "მორგებული ფონტის პარამეტრების გამოყენება"

#. i18n: ectx: property (text), widget (QLabel, fontFamilyLabel)
#: appearance-config.ui:184
#, kde-format
msgid "Select Font:"
msgstr "აირჩიეთ ფონტი:"

#. i18n: ectx: property (text), widget (QLabel, fontSizeLabel)
#: appearance-config.ui:194
#, kde-format
msgid "Select Size:"
msgstr "აირჩიეთ ზომა:"
