# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Yuri Efremov <yur.arh@gmail.com>, 2012, 2013.
# Alexander Lakhin <exclusion@gmail.com>, 2013.
# Alexander Potashev <aspotashev@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2015-05-06 14:05+0300\n"
"Last-Translator: Alexander Potashev <aspotashev@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: appearance-config-tab.cpp:44
#, kde-format
msgid "A demo chat"
msgstr "Образец разговора"

#: appearance-config-tab.cpp:45
#, kde-format
msgid "Jabber"
msgstr "Jabber"

#: appearance-config-tab.cpp:47 appearance-config-tab.cpp:196
#: appearance-config-tab.cpp:203 appearance-config-tab.cpp:249
#: appearance-config-tab.cpp:265
#, kde-format
msgctxt "Example email"
msgid "ted@example.com"
msgstr "vasya@example.com"

#: appearance-config-tab.cpp:48
#, kde-format
msgctxt "Example name"
msgid "Ted"
msgstr "Василий"

#: appearance-config-tab.cpp:181
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Ok!"
msgstr "Хорошо!"

#: appearance-config-tab.cpp:182 appearance-config-tab.cpp:189
#: appearance-config-tab.cpp:232 appearance-config-tab.cpp:240
#, kde-format
msgctxt "Example email"
msgid "larry@example.com"
msgstr "semyon@example.com"

#: appearance-config-tab.cpp:183 appearance-config-tab.cpp:190
#: appearance-config-tab.cpp:233 appearance-config-tab.cpp:241
#, kde-format
msgctxt "Example name"
msgid "Larry Demo"
msgstr "Семён"

#: appearance-config-tab.cpp:188
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Bye Bye"
msgstr "Пока"

#: appearance-config-tab.cpp:195
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Have fun!"
msgstr "Удачи!"

#: appearance-config-tab.cpp:197 appearance-config-tab.cpp:204
#: appearance-config-tab.cpp:210 appearance-config-tab.cpp:217
#: appearance-config-tab.cpp:224 appearance-config-tab.cpp:250
#: appearance-config-tab.cpp:266 appearance-config-tab.cpp:273
#, kde-format
msgctxt "Example name"
msgid "Ted Example"
msgstr "Василий"

#: appearance-config-tab.cpp:202
#, kde-format
msgctxt "Example message in preview conversation"
msgid "cya"
msgstr "До скорого!"

#: appearance-config-tab.cpp:209
#, kde-format
msgctxt "Example message"
msgid "Ted Example waves."
msgstr "Василий машет рукой."

#: appearance-config-tab.cpp:216
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Ted Example has left the chat."
msgstr "Василий покидает разговор"

#: appearance-config-tab.cpp:223
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Ted Example has joined the chat."
msgstr "Василий присоединяется к разговору"

#: appearance-config-tab.cpp:231
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Hello Ted"
msgstr "Привет, Василий"

#: appearance-config-tab.cpp:239
#, kde-format
msgctxt "Example message in preview conversation"
msgid "What's up?"
msgstr "Как дела?"

#: appearance-config-tab.cpp:246
#, kde-format
msgctxt "Example message in preview conversation"
msgid ""
"Check out which cool adium themes work <a href=\"http://community.kde.org/"
"KTp/Components/Chat_Window/Themes\">here</a>!"
msgstr ""
"Нашёл <a href=\"http://community.kde.org/KTp/Components/Chat_Window/Themes"
"\">здесь</a> классные темы от Adium, которыми можно воспользоваться!"

#: appearance-config-tab.cpp:256
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Hello"
msgstr "Привет"

#: appearance-config-tab.cpp:257
#, kde-format
msgctxt "Example email"
msgid "bob@example.com"
msgstr "vitaly@example.com"

#: appearance-config-tab.cpp:258
#, kde-format
msgctxt "Example name"
msgid "Bob Example"
msgstr "Виталий"

#: appearance-config-tab.cpp:264
#, kde-format
msgctxt "Example message in preview conversation"
msgid "A different example message"
msgstr "Другой пример сообщения"

#: appearance-config-tab.cpp:272
#, kde-format
msgctxt "Example message in preview conversation"
msgid "Ted Example is now Away."
msgstr "Василий отсутствует"

#: appearance-config.cpp:47
#, kde-format
msgctxt "@title:tab"
msgid "Normal Chat"
msgstr "Обычный разговор"

#: appearance-config.cpp:51
#, kde-format
msgctxt "@title:tab"
msgid "Group Chat"
msgstr "Групповой разговор"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: appearance-config.ui:41
#, kde-format
msgid "Style"
msgstr "Стиль"

#. i18n: ectx: property (text), widget (QLabel, styleLabel)
#: appearance-config.ui:52
#, kde-format
msgid "Message Style:"
msgstr "Стиль сообщения:"

#. i18n: ectx: property (text), widget (QLabel, variantLabel)
#: appearance-config.ui:78
#, kde-format
msgid "Variant:"
msgstr "Вариант:"

#. i18n: ectx: property (text), widget (QCheckBox, showHeader)
#: appearance-config.ui:113
#, kde-format
msgid "Show Header"
msgstr "Показывать заголовок"

#. i18n: ectx: property (text), widget (QCheckBox, showPresenceCheckBox)
#: appearance-config.ui:129
#, kde-format
msgid "Show status changes"
msgstr "Показывать изменения статуса"

# Можно было бы перевести лучше -- "Показывать появление и исчезновение контактов в сети", но приходится экономить пространство в окне. --aspotashev
#. i18n: ectx: property (text), widget (QCheckBox, showJoinLeaveCheckBox)
#: appearance-config.ui:142
#, kde-format
msgid "Show when contacts join/leave chat"
msgstr "Показывать вход/выход контактов"

#. i18n: ectx: property (title), widget (QGroupBox, customFontBox)
#: appearance-config.ui:170
#, kde-format
msgid "Use Custom Font Settings"
msgstr "Использовать другой шрифт:"

#. i18n: ectx: property (text), widget (QLabel, fontFamilyLabel)
#: appearance-config.ui:184
#, kde-format
msgid "Select Font:"
msgstr "Выбор шрифта:"

#. i18n: ectx: property (text), widget (QLabel, fontSizeLabel)
#: appearance-config.ui:194
#, kde-format
msgid "Select Size:"
msgstr "Выбор размера:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Юрий Ефремов,Александр Лахин,Александр Поташев"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "yur.arh@gmail.com,exclusion@gmail.com,aspotashev@gmail.com"

#~ msgid "Always"
#~ msgstr "Всегда"

#~ msgid "Never in group chats"
#~ msgstr "Никогда в групповых чатах"

#~ msgid "Never"
#~ msgstr "Никогда"
